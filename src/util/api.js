export const login = '/cert/accounts/login';
export const signup = '/cert/accounts/signup';
export const logout = '/cert/accounts/logout';
export const checkLogin = '/cert/api/check/login';
export const signupEmail = '/cert/accounts/emailCode/signup'
export const passwordResetEmail = '/cert/accounts/emailCode/passwordReset'
export const passwordReset = '/cert/accounts/passwordReset'

export const order = '/cert/api/cert'
export const orderBase = '/cert/api/cert/'