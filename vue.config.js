module.exports = {
  outputDir: 'dist/html',
  devServer: {
    open: false, //是否自动弹出浏览器页面
    https: false,
    proxy: {
      '/cert': {
        target:  'http://127.0.0.1:8080',//'http://54.238.255.126:8080/', //'http://127.0.0.1:8083',  //API服务器的地址
        ws: true,  //代理websockets
        // changeOrigin: true, // 虚拟的站点需要更管origin
        pathRewrite: {   //重写路径 比如'/api/aaa/ccc'重写为'/aaa/ccc'
          '^/cert': ''
        },
      }
    }
  }
}